package com.codecompiler.shikhon.trainingone;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.Holder> {
    Context context;
    List<USER> list;

    public Adapter(Context context, List<USER> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(context).inflate(R.layout.layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int position) {

        holder.name.setText(list.get(position).getName());
        holder.mobile.setText(list.get(position).getMobile());
        holder.imageView.setImageDrawable(list.get(position).getImage());

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle=new Bundle();
                bundle.putString("name",list.get(position).getName());
                bundle.putString("mobile",list.get(position).getMobile());
//                bundle.put("image",list.get(position).getImage());
                Fragment frag=new DetailFragment();
                frag.setArguments(bundle);
                ((MainActivity)context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_home, frag, "detail").addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView name,mobile;
        ImageView imageView;
        LinearLayout layout;
        public Holder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.tv_name_layout);
            mobile=itemView.findViewById(R.id.tv_mobile_layout);
            imageView=itemView.findViewById(R.id.iv_layout);
            layout=itemView.findViewById(R.id.layoutMain);
        }
    }
}
