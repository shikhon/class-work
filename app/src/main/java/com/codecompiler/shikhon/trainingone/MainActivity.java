package com.codecompiler.shikhon.trainingone;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static com.codecompiler.shikhon.trainingone.HideKeyBoard.hideKeyboard;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener,
        DetailFragment.OnFragmentInteractionListener {
    EditText etmobile, etpassword;
    String mobile, password;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etmobile = findViewById(R.id.et_home_mobile);
        etpassword = findViewById(R.id.et_home_password);
        btnLogin = findViewById(R.id.btn_home_log_in);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etmobile.setError(null);
                etpassword.setError(null);
                mobile = "+88" + etmobile.getText().toString();
                password = etpassword.getText().toString();

                if (mobile.length() < 14) etmobile.setError("Enter a valid mobile number");
                else if (password.isEmpty()) etpassword.setError("Enter your password");
                else LogIN();
            }
        });


    }

    private void LogIN() {
        hideKeyboard(MainActivity.this);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_home, new HomeFragment(), "home").addToBackStack(null)
                .commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
