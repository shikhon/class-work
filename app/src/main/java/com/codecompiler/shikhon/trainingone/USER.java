package com.codecompiler.shikhon.trainingone;

import android.graphics.drawable.Drawable;

public class USER {
    private String name,mobile;
    private Drawable image;

    public USER() {
    }

    public USER(String name, String mobile, Drawable image) {
        this.name = name;
        this.mobile = mobile;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }
}
